<?php

$numeros = [1,4,5,6,7,8,9];

echo "for";
// for
for($c=0;$c<count($numeros);$c++){
?>   
   <li><?= $c . ":" . $numeros[$c] ?></li>
<?php
}

echo "for";
for($c=0;$c<count($numeros);$c++){
    echo "<li>$numeros[$c]</li>";
}

$c = 0;

echo "while";
//while
while($c<count($numeros)){
    echo "<li>$c:$numeros[$c]</li>";
    $c++;
}

echo "foreach";
//foreach
foreach($numeros as $indice=>$valor){
    echo "<li>$indice:$valor</li>";
}
